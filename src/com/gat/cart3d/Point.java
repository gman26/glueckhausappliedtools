/*
 *  Copyright [2013] [Glueckhaus]
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.gat.cart3d;

/**
 * Three dimensional cartesian representation of a position in space. Rotations
 * around primary axis and common subsequent rotations.
 * <br>
 * Requires an x, y, and z coordinate.
 *
 * @author zach
 */
public class Point {

    private double x;
    private double y;
    private double z;
    private double precision = 1E-12f;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Point(double x, double y, double z, double precision) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.precision = precision;
    }
    
    
    /**
     * Rotate the point around the x-axis.
     *
     * @param rad rotation value around x-axis in radians
     */
    public void rotX(double rad) {
        double nx = 1f * x + 0f * y + 0f * z;
        double ny = 0f * x + Math.cos(rad) * y - Math.sin(rad) * z;
        double nz = 0f * x + Math.sin(rad) * y + Math.cos(rad) * z;
        
        x = nx;
        y = ny;
        z = nz;
    }
    
    /**
     * Rotate the point around the y-axis.
     *
     * @param rad rotation value around y-axis in radians
     */
    public void rotY(double rad) {
        double nx = Math.cos(rad) * x + 0f * y + Math.sin(rad) * z;
        double ny = 0f * x + 1f * y + 0f * z;
        double nz = -Math.sin(rad) * x + 0f * y + Math.cos(rad) * z;
        
        x = nx;
        y = ny;
        z = nz;
    }
    
    /**
     * Rotate the point around the z-axis.
     *
     * @param rad rotation value around y-axis in radians
     */
    public void rotZ(double rad) {
        double nx = Math.cos(rad) * x - Math.sin(rad) * y + 0f * z;
        double ny = Math.sin(rad) * x + Math.cos(rad) * y + 0f * z;
        double nz = 0f * x + 0f * y + 1f * z;
        
        x = nx;
        y = ny;
        z = nz;
    }
    
    /**
     * Rotate the point subsequently around the axis
     *
     * @param zRad
     * @param yRad
     * @param xRad
     */
    public void rotZYX(double zRad, double yRad, double xRad) {
        
        double[][] mZ = matRotZ(zRad);
        double[][] mY = matRotY(yRad);
        double[][] mX = matRotX(xRad);
        
        double[][] xyProduct = matrixMultiply(mX,mY);
        double[][] m = matrixMultiply(xyProduct,mZ);
        
        double nx = x*m[0][0]+y*m[1][0]+z*m[2][0];
        double ny = x*m[0][1]+y*m[1][1]+z*m[2][1];
        double nz = x*m[0][2]+y*m[1][2]+z*m[2][2];
        
        x = nx;
        y = ny;
        z = nz;

    }
    
    /**
     * Multiply two 3x3 Matrices
     *
     * @param mX
     * @param mY
     */
    private static double[][] matrixMultiply(double[][] mX, double[][] mY) {
        double[][] xyProduct = {
            {mX[0][0]*mY[0][0]+mX[0][1]*mY[1][0]+mX[0][2]*mY[2][0],mX[0][0]*mY[0][1]+mX[0][1]*mY[1][1]+mX[0][2]*mY[2][1],mX[0][0]*mY[0][2]+mX[0][1]*mY[1][2]+mX[0][2]*mY[2][2]},
            {mX[1][0]*mY[0][0]+mX[1][1]*mY[1][0]+mX[1][2]*mY[2][0],mX[1][0]*mY[0][1]+mX[1][1]*mY[1][1]+mX[1][2]*mY[2][1],mX[1][0]*mY[0][2]+mX[1][1]*mY[1][2]+mX[1][2]*mY[2][2]},
            {mX[2][0]*mY[0][0]+mX[2][1]*mY[1][0]+mX[2][2]*mY[2][0],mX[2][0]*mY[0][1]+mX[2][1]*mY[1][1]+mX[2][2]*mY[2][1],mX[2][0]*mY[0][2]+mX[2][1]*mY[1][2]+mX[2][2]*mY[2][2]}
        };
        return xyProduct;
    }
    
    /**
     * Get specified axis rotation matrix
     *
     * @param ang
     */
    private static double[][] matRotZ(double ang) {
        double[][] z = {   
            {Math.cos(ang),Math.sin(ang),0f},
            {-Math.sin(ang),Math.cos(ang),0f},
            {0f,0f,1f}
        };
        return z;
    }
    
    /**
     * Get specified axis rotation matrix
     *
     * @param ang
     */
    private static double[][] matRotY(double ang) {
        double[][] y = {   
            {Math.cos(ang),0f,-Math.sin(ang)},
            {0f,1f,0f},
            {Math.sin(ang),0f,Math.cos(ang)}
        };
        return y;
    }
    
    /**
     * Get specified axis rotation matrix
     *
     * @param ang
     */
    private static double[][] matRotX(double ang) {
        double[][] x = {   
            {1f,0f,0f},
            {0f,Math.cos(ang),Math.sin(ang)},
            {0f,-Math.sin(ang),Math.cos(ang)}
        };
        return x;
    }
    
    /**
     * Rotate the point subsequently around the axis
     *
     * @param xOneRad
     * @param yRad
     * @param xTwoRad
     */
    public void rotXYX(double xOneRad, double yRad, double xTwoRad) {
        
        double[][] xOne = matRotX(xOneRad);
        double[][] yRot = matRotY(yRad);
        double[][] xTwo = matRotZ(xTwoRad);
        
        double[][] xyProduct = matrixMultiply(xOne,yRot);
        double[][] m = matrixMultiply(xyProduct,xTwo);
        
        double nx = x*m[0][0]+y*m[1][0]+z*m[2][0];
        double ny = x*m[0][1]+y*m[1][1]+z*m[2][1];
        double nz = x*m[0][2]+y*m[1][2]+z*m[2][2];
        
        x = nx;
        y = ny;
        z = nz;
    }
    
    /**
     * Extract Euler Angles from vector with YPR rotation as an Array of Yaw, Pitch, Roll.
     *
     * @param PointingVector
     * @param OrientationVector
     */
    public static double[] getEulerAngles(double[] p, double[] o) {
        
        return null;

    }

    /**
     * Get the value of x
     *
     * @return the value of x
     */
    public double getX() {
        return x;
    }

    /**
     * Set the value of x
     *
     * @param x new value of x
     */
    public void setX(double x) {
        this.x = x;
    }

    /**
     * Get the value of y
     *
     * @return the value of y
     */
    public double getY() {
        return y;
    }

    /**
     * Set the value of y
     *
     * @param y new value of y
     */
    public void setY(double y) {
        this.y = y;
    }

    /**
     * Get the value of z
     *
     * @return the value of z
     */
    public double getZ() {
        return z;
    }

    /**
     * Set the value of z
     *
     * @param z new value of z
     */
    public void setZ(double z) {
        this.z = z;
    }

    /**
     * Get the value of precision
     *
     * @return the value of precision
     */
    public double getPrecision() {
        return precision;
    }

    /**
     * Set the value of precision
     *
     * @param precision new value of precision
     */
    public void setPrecision(double precision) {
        this.precision = precision;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }
        final Point other = (Point) obj;

        double dr = Math.sqrt((other.x - x) * (other.x - x) + (other.y - y) * (other.y - y) + (other.z - z) * (other.z - z));

        if (dr < precision) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public String toString() {
        return "Point{" + "x=" + x + ", y=" + y + ", z=" + z + '}';
    }
    
}
