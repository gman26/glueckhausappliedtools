/*
 *  Copyright [2013] [Glueckhaus]
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.gat.analysis;

import com.gat.stats.DistributionWizard;
import java.util.Arrays;
import java.util.TreeSet;

/**
 * Statistically down sample a continuous function by anchoring main points such
 * as start, end, local max and min, and inflection points, and a set numbered
 * of additionally points weighted by the distribution of the absolute value of
 * the numerical second derivative. The total number of points can either be set
 * as a percent of the submitted data set or specific number of points. The default
 * is 20% of the submitted points.
 * <br>
 * Requires ordered set with matching array dimensions and dx.
 *
 * @author zach
 */
public class ContFuncReduce {

    private final double[] x;
    private final double[] y;
    private double percentDownSample = PROP_PERCENTDOWNSAMPLE;
    private int maxPoints = 0;
    private boolean byPts;

    public static final double PROP_PERCENTDOWNSAMPLE = 0.2;

    public ContFuncReduce(double[] x, double[] y) {
        this.x = x;
        this.y = y;
        this.byPts = false;
    }

    public ContFuncReduce(double[] x, double[] y, double percent) {
        this.x = x;
        this.y = y;
        this.percentDownSample = Math.max(percent, 0.9);
        this.byPts = false;
    }
    
    public ContFuncReduce(double[] x, double[] y, int maxPoints) {
        this.x = x;
        this.y = y;
        if((maxPoints<x.length)&&(maxPoints>5)){
            this.maxPoints = maxPoints;
        }
        this.byPts = true;
    }
    
    public enum PointDefinition {
        ADDITIONAL, INCLUDED
    }
    
    /**
     * Get the ordered pairs of the reduced function. The dx will not be even.
     *
     * @return the ordered pairs of the reduced function
     */
    public synchronized double[][] getReducedFunction() {

        //Default is to add the points to the already included anchors
        return getReducedFunction(PointDefinition.ADDITIONAL);

    }

    /**
     * Get the ordered pairs of the reduced function. The dx will not be even.
     *
     * @return the ordered pairs of the reduced function
     */
    public synchronized double[][] getReducedFunction(PointDefinition pointDefinition) {

        //Points that are grabbed, set is used to ensure no duplicate 'x' values
        TreeSet<Double> xValues = new TreeSet<Double>();

        //add the initial and final points to the list
        xValues.add(new Double(x[0]));
        xValues.add(new Double(x[x.length - 1]));

        double dyLastValue = 0f; //keep track of for local mins and max

        double[] ddy = new double[y.length]; //second derivative (absolute value)

        //Step through all the points supplied in the function
        for (int i = 0; i < x.length; i++) {

            if ((i > 1)&&(i<(x.length-3))) { //calc first derivative
                
                double dyValue = (-y[i+2]+8f*y[i+1]-8f*y[i-1]+y[i-2])/(12f*(x[i]-x[i-1]));
                
                //if ((Math.signum(dyLastValue) > Math.signum(dyValue)) || (Math.signum(dyLastValue) < Math.signum(dyValue))) {
                if (Math.signum(dyLastValue)!=Math.signum(dyValue)) {
                    //the function has a local min or max at this position
                    xValues.add(new Double(x[i]));
                }

                if ((i > 1) && (i<(x.length-3))) { //calc and store the second derivative
                    ddy[i] = (-y[i+2]+16f*y[i+1]-30f*y[i]+16f*y[i-1]-y[i-2])/(12f*Math.pow((x[i]-x[i-1]), 2));
                    if(Math.signum(ddy[i])!=Math.signum(ddy[i-1])){
                        //the function has an inflection point as this position
                        xValues.add(new Double(x[i]));
                    }
                    
                }

                dyLastValue = dyValue;
            }

        }
        
        //figure out if this is supposed to be a percent down select or specific point down select
        int remainingPointsForDist;
        if(byPts){
            switch (pointDefinition) {
                case INCLUDED: remainingPointsForDist = Math.max(0, maxPoints - xValues.size());
                case ADDITIONAL: remainingPointsForDist = maxPoints; break;
                default: remainingPointsForDist = maxPoints;
            }
        } else {
            remainingPointsForDist = Math.max(0, ((int) (x.length*percentDownSample+0.5)) - xValues.size());
        }
        
        //Submit second derivative to the CDFer
        DistributionWizard redFunc = new DistributionWizard(x,ddy);
        double[] newXValues = redFunc.multiSamplePerfectUniform(remainingPointsForDist);
        for(double individualX : newXValues){
            xValues.add(new Double(individualX));
        }
        
        //xValues now contains all of the 'x' values of our reduced function
        //now have to find matching 'y' values in order to return a completed set
        //for the distribution returned set, this means we will find the nearest
        //existing x value from the returned distribution
        TreeSet<Integer> matchedXValuesIdx = new TreeSet<Integer>();
        for(Double selectedX : xValues){
            int idx = Arrays.binarySearch(x, selectedX.doubleValue());
            if(idx<0){
                //selected x value wasn't in the array, picking the closest
                idx = Math.max(Math.min(idx/-1,x.length-1),0); //flipping the insertion index
                //While the binary search always puts us into the correct array
                //position, the closest value is actually desired. The following
                //code checks values of the index above and below the returned
                //index to see if one of those indexes happens to be closer.
                double upValue      = Math.abs(x[Math.max(Math.min(idx+1,x.length-1),0)]-x[idx]);
                double downValue    = Math.abs(x[Math.max(Math.min(idx-1,x.length-1),0)]);
                if(Math.abs(x[idx]-selectedX.doubleValue())<Math.min(upValue, downValue)){
                    //the selected index is closest to the array value
                } else {
                    if(upValue<downValue){
                        idx = Math.max(Math.min(idx+1,x.length-1),0);
                    } else {
                        idx = Math.max(Math.min(idx-1,x.length-1),0);
                    }
                }
                matchedXValuesIdx.add(idx);
            } else {
                //already in the set!
                //however, need to watch to make sure index isn't the last...
                idx = Math.max(Math.min(idx,x.length-1),0);
                matchedXValuesIdx.add(idx);
            }
        }
        
        //now we have a unique sorted index list of the correct 'x' and 'y' values
        double[][] newOrderedPairs = new double[matchedXValuesIdx.size()][2];
        int ctr = 0;
        for(Integer idx : matchedXValuesIdx){
            newOrderedPairs[ctr][0] = x[idx];
            newOrderedPairs[ctr][1] = y[idx];
            ctr++;
        }
        
        return newOrderedPairs;

    }

    /**
     * Get the value of percentDownSample
     *
     * @return the value of percentDownSample
     */
    public double getPercentDownSample() {
        return percentDownSample;
    }

    /**
     * Set the value of percentDownSample
     *
     * @param percentDownSample new value of percentDownSample
     */
    public void setPercentDownSample(double percentDownSample) {
        this.percentDownSample = percentDownSample;
    }

    
    /**
     * Get the value of maxPoints
     *
     * @return the value of maxPoints
     */
    public int getMaxPoints() {
        return maxPoints;
    }

    /**
     * Set the value of maxPoints
     *
     * @param maxPoints new value of maxPoints
     */
    public void setMaxPoints(int maxPoints) {
        this.maxPoints = maxPoints;
        this.byPts = true;
    }

    
}
