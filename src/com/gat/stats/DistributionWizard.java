/*
 *  Copyright [2013] [Glueckhaus]
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.gat.stats;

import java.util.Arrays;
import java.util.Random;

/**
 * Use any continuous function (represented by ordered set x and y)
 * to build a distribution and sample from the distribution.
 * <br>
 * Requires ordered set with matching array dimensions and dx.
 * @author zach
 */
public class DistributionWizard {

    private double[] x;
    private double[] y;
    private double[][] cdf;
    private double[] idxCdf;  //Easily searchable cdf index
    private Random r = new Random();

    public DistributionWizard(double[] x, double[] y) {
        this.x = x;
        this.y = y;
    }
    
    public DistributionWizard(double[] x, double[] y, int randSeed) {
        this.x = x;
        this.y = y;
        this.r = new Random(randSeed);
    }
    
    /**
     * Get the ordered pairs of the distributions CDF.
     *
     * @return x and y pair values of the distributions CDF.
     */
    public double[][] getCDF() {
        
        double cumulativeSum = 0f;
        double dx = x[1] - x[0];
        
        for(int i = 0; i<(x.length-1); i++){
            //Trapezoidal Integration
            //cumulativeSum += (Math.abs(y[i+1]) + Math.abs(y[i])) * 0.5 * dx;
            cumulativeSum += (y[i+1] + y[i]) * 0.5; //why?
        }
        
        double[][] pCdf  = new double[x.length][2];
        idxCdf  = new double[x.length];
        pCdf[0][0] = x[0];
        
        for(int i = 1; i<x.length; i++){
            pCdf[i][0] = x[i];
            double sumC = pCdf[i-1][1] + (y[i]/cumulativeSum);
            idxCdf[i] = sumC;
            pCdf[i][1] = sumC;
        }
        
        if(cdf==null){
            cdf = pCdf;
        }
        
        return pCdf;
        
    }
    
    /**
     * Get a sample of the distribution.
     *
     * @return returns the 'x' sample value of given distribution.
     */
    public synchronized double sample() {
        
        double val = r.nextDouble();
        
        return sample(val);
        
    }
    
     /**
     * Get a sample of the distribution.
     *
     * @param v the point on the cdf that you want to sample (must be between 0 and 1)
     * @return returns the 'x' sample value of given distribution.
     */
    public synchronized double sample(double v) {
        
        if(cdf==null){
            getCDF();
        }
        
        int i = Arrays.binarySearch(idxCdf, v);
        i = (i<0) ? (Math.abs(i) - 1) : i;

        return (cdf[i-1][0]+(cdf[i][0]-cdf[i-1][0])*((v-cdf[i-1][1])/(cdf[i][1]-cdf[i-1][1])));
        
    }
    
    /**
     * Get multiple (n) samples of the distribution.
     *
     * @return returns the 'x' sample value of given distribution.
     */
    public synchronized double[] multiSample(int n) {
        
        double ret[] = new double[n];
        
        for(int i = 0; i < ret.length; i++){
            ret[i] = sample();
        }
        
        return ret;
        
    }
    
     /**
     * Get multiple (n) samples of the distribution. Sampling will be from perfect
     * uniform distribution with identical values between samples (i.e. 0.1,0.2,0.3,...)
     * To avoid numerically instability the first and last values are not used. For
     * a sample between 0 and 1 of 5 points the points would be: 1/6,1/3,1/2,2/3,5/6
     *
     * @return returns the 'x' sample value of given distribution.
     */
    public synchronized double[] multiSamplePerfectUniform(int n) {
        
        double ret[] = new double[n];
        
        //figure out the start and end points of sampling and the even delta
          double step = 1f/(n+1f);
        
        for(int i = 0; i < ret.length; i++){
            ret[i] = sample(step+i*step);
        }
        
        return ret;
        
    }
    


}
